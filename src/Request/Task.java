package Request;

import java.util.ArrayList;
import java.util.List;

import Files.Data;

public abstract class Task 
{ 
	protected Task next  = null; 
	

	List<Data> inputs = new ArrayList<Data>();  
	List<Data> outputs = new ArrayList<Data>();
	 
	
	public Task getNextTask() {
		return next;
	}
	public void setNextTask(Task t) {
		this.next = t;
	} 

	public abstract void execut(); 
	public abstract void display(); 
	
	

	public void addOutput(Data out) {
		this.outputs.add(out);
	}
	public void addInput(Data in) {
		this.outputs.add(in);
	}
	
	public List<Data> getInputs() {
		return inputs;
	}
	
	public List<Data> getOutputs() {
		return outputs;
	}
	
	public void setOutputs(List<Data> outputs) {
		this.outputs = outputs;
	}
	
	public void setInputs(List<Data> inputs) {
		this.inputs = inputs;
	}
}
