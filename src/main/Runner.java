package main;

import java.util.ArrayList;
import java.util.Collection;

import Request.Task;

public class Runner extends Thread
{
	Collection<Task> tasks = new ArrayList<>();
	private boolean runing; 
	
	public void addTask(Task task) {
		this.tasks.add(task);
	}
	
	@Override
	public void run() { 
		super.run();
		runing = true;

		Task lastTask = null;
		for(Task t : tasks)
		{
			if(lastTask != null) {
				t.setInputs( lastTask.getOutputs() );
			}
			t.execut(); 
			lastTask = t; 
		}

		System.out.println("Runner of "+tasks.size()+" tasks !");
		lastTask.display();
	}
}
