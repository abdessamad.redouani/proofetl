package Files;


import java.io.File;

public interface IFichier {
	public File loadFile();
	public void display();
}
